<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

route::get('/sanbercode','HomeController@Index');
route::get('/register','AuthController@register');
route::get('/welcome', 'AuthController@form');
route::get('/',function(){
    return view('awal');
});
route::get('/table',function(){
    return view('table');
});
route::get('/data-table', function (){
    return view('data-table');
});